# README #

### What is this repository for? ###

This is an Android client for Brewery Controller
https://bitbucket.org/pmarcinkowski/raspberry-pi-brewery-controller/

### Application screenshots ###

![Screens from the application](docs/screenshot.png)
